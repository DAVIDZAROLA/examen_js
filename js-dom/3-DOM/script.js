'use stric';

function reloj() {
  let date = new Date();
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let second = date.getSeconds();
  if (hour < 10) hour = '0' + hour;
  if (minutes < 10) minutes = '0' + minutes;
  if (second < 10) second = '0' + second;

  if (second % 2 === 0) {
    hora.textContent = `${hour}:${minutes}:${second}`;
  } else {
    hora.textContent = `${hour}:${minutes} ${second}`;
  }
}

const hora = document.createElement('div');
hora.textContent = hora;
document.body.appendChild(hora);

setInterval(() => {
  reloj();
}, 1000);

/**ASI SERIA SIN CREAR LA FUNCION RELOJ**

  const hora = document.createElement('div');
  hora.textContent = hora;
  document.body.appendChild(hora);
  
  setInterval(() => {
    let date = new Date();
    let hour = date.getHours();
    let minutes = date.getMinutes();
    let second = date.getSeconds();
    if (hour < 10) hour = '0' + hour;
    if (minutes < 10) minutes = '0' + minutes;
    if (second < 10) second = '0' + second;

    if (second % 2 === 0) {
      hora.textContent = `${hour}:${minutes}:${second}`;
    } else {
      hora.textContent = `${hour}:${minutes} ${second}`;
    }
  }, 1000);
 */
